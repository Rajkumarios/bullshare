//
//  ETFStatisticsVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 19/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit
import Charts

class ETFStatisticsVC: BTBaseViewController {
    
    //  Variables decleration...
    var tickerName: String?
    var duration: String?
    var durationSelected: Bool = false
    private let graphs_cell = "GraphsCell"
    let months = ["Jan", "Feb", "Mar",
                  "Apr", "May", "Jun",
                  "Jul", "Aug", "Sep",
                  "Oct", "Nov", "Dec"]
    
    private let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                             Headers.contentType: ContentType.Json]
    //  IBOutlets decleration...
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var tickerLbl: UILabel!
    @IBOutlet weak var exchangeLbl: UILabel!
    @IBOutlet weak var openPriceLbl: UILabel!
    @IBOutlet weak var closePriceLbl: UILabel!
    @IBOutlet weak var highLbl: UILabel!
    @IBOutlet weak var lowLbl: UILabel!
    @IBOutlet weak var netChangeLbl: UILabel!
    @IBOutlet weak var volumeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var annualDividendLbl: UILabel!
    @IBOutlet weak var YtdGainLbl: UILabel!
    @IBOutlet weak var YearGainLbl: UILabel!
    @IBOutlet weak var dayGainLbl: UILabel!
    
    @IBOutlet weak var graphPageControl: UIPageControl!
    @IBOutlet weak var graphsCV: UICollectionView!
    
    @IBOutlet var chartsFiltersView: UIView!
    @IBOutlet weak var chartFilterSubView: BTViewTopCorners!
    @IBOutlet var durationBtns: [BTButtonShadow]!
    
    //  View Life-cycles decleration...
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        getDelayedData()
    }
    
    func updateUI(){
     
       chartsFiltersView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        chartsFiltersView.isHidden = false
        self.view.addSubview(chartsFiltersView)
        dragChartFilterSubView(subView: chartFilterSubView)
    }
    func dragChartFilterSubView(subView: UIView){
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureDragged))
        subView.addGestureRecognizer(panGesture)
    }
    @objc func panGestureDragged(){
        UIView.animate(withDuration: 2) {
            self.chartsFiltersView.isHidden = true
        }
    }
    
    
    // Get Delayed Data form server...
    func getDelayedData(){
        
        let postParams =
            ["ticker": tickerName ?? "",
             "token": User.accessToken]
        
        serviceWithToken(headers: headers, parmas: postParams, url: API.KDelayed, type: Statistics.self) { (etf) in
            guard let status = etf.status, let statusCode = status.code , let message = status.message else{return}
            if statusCode == 200{
                guard let etfData = etf.data else{return}
                
                // Get  Gain-data service call...
                self.getGainData()
                DispatchQueue.main.async {
                    self.updateUIFrom(etfDetails: etfData[0])
                }
            }else{
                print(message)
            }
        }
    }
    
    // UI update...
    func updateUIFrom(etfDetails: Stats){
        timeLbl.text        = etfDetails.tradeTime
        dateLbl.text        =  converMillsecToDate(etfDetails.dt)
        tickerLbl.text      = etfDetails.ticker
        exchangeLbl.text    = etfDetails.exchange
        openPriceLbl.text   = "$\(etfDetails.open ?? " ")"
        closePriceLbl.text  = "$\(etfDetails.last ?? " ")"
        netChangeLbl.text   = "\(etfDetails.percentChange ?? " ")%"
        volumeLbl.text      = "\(etfDetails.volume ?? " ")"
        highLbl.text        = etfDetails.high
        lowLbl.text         = etfDetails.low
    }
    
    func converMillsecToDate(_ sec: Int?) -> String{
        let date = Date(timeIntervalSince1970: (TimeInterval(sec! / 1000)))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        return dateFormatter.string(from: date)
    }
    
    // Get anual Dividend from Server...
    func getGainData(){
        
        let postParams =
            ["ticker": tickerName ?? "",
             "token": User.accessToken]
        
        serviceWithToken(headers: headers, parmas: postParams, url: API.KGain, type: Gain.self) { (gain) in
            guard let status = gain.status, let statusCode = status.code , let message = status.message else{return}
            if statusCode == 200{
                guard let gainData = gain.data else{return}
                
                if gainData.count == 0{
                    return
                }
                DispatchQueue.main.async {
                    self.updateUIFrom(gainData: gainData[0])
                }
            }else{
                print(message)
            }
        }
    }
    
    @IBAction func onChartsfilterBtnTapped(_ sender: UIButton) {
        updateUI()
    }
    @IBAction func durationBtnsTapped(_ sender: BTButtonShadow) {
        durationSelected = true
        switch sender.tag {
        case 1:
            sender.layer.borderColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
            durationBtns[1].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[2].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[3].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[4].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        case 2:
            sender.layer.borderColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
            durationBtns[0].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[2].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[3].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[4].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        case 3:
            sender.layer.borderColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
            durationBtns[0].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[1].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[3].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[4].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        case 4:
            sender.layer.borderColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
            durationBtns[0].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[1].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[2].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[4].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        case 5:
            sender.layer.borderColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
            durationBtns[0].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[1].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[2].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            durationBtns[3].layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        default:
            sender.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        }
        
    }
    @IBAction func onCheckBtnTapped(_ sender: UIButton) {
        if durationSelected == true{
            self.chartsFiltersView.isHidden = true

        }else{
            showWarnig(title: "Filters", message: "Please select duration before going to check")
        }
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
    }
    @IBAction func newsBtnTapped(_ sender: BTButton) {
        let newsVCRef = storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTNewsVc) as! BTNewsVC
        newsVCRef.tickerName = tickerName
        navigateTo(viewController: newsVCRef)
    }
    
    // Update UI...
    func updateUIFrom(gainData: GainData){
        annualDividendLbl.text  = "ANNUAL DIVIDEND : $\(gainData.dividend ?? 0.00)"
        YtdGainLbl.text         = "$\(gainData.ytdGain ?? 0.00)"
        YearGainLbl.text        = "$\(gainData.yearGain ?? 0.00)"
        dayGainLbl.text         = "$\(gainData.day30Gain ?? 0.00)"
    }
    
}

// MARK: CHARTS DATA SOURCE AND DELEGATES...
extension ETFStatisticsVC: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        graphPageControl.numberOfPages = 4
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: graphs_cell  , for: indexPath) as! GraphsCell
        
        cell.graphView.delegate = self
        cell.graphView.layer.cornerRadius = 8
        cell.graphView.clipsToBounds = true
        cell.graphView.backgroundColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        
        
        switch indexPath.row {
        case 0:
            compareTickersData(cell: cell)
        case 1:
            showAroonChart(cell: cell)
        case 2:
            showVolumeChart(cell: cell)
        case 3:
            showPriceChart(cell: cell)
        default:
            print("No Charts")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: graphsCV.frame.width, height: graphsCV.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        switch index {
        case 0:
            let value = UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        default:
            print("no orientation")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        
        if let currentPage = graphsCV.indexPathForItem(at: center) {
            graphPageControl.currentPage = currentPage.row
        }
    }
    
}

// MARK: DISPLAY CHARTS...
extension ETFStatisticsVC: ChartViewDelegate,IAxisValueFormatter{
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value) % months.count]
        
    }
    // Get Compare charts...
    func compareTickersData(cell: GraphsCell){
        
        var legend = Legend()
        var xAxis = XAxis()
        var leftAxis = YAxis()
        var dataEntries = [ChartDataEntry]()
        var dataTwoEntries = [ChartDataEntry]()
        
        let postParams = ["ticker": tickerName ?? "",
                          "ticker2": "VOO",
                          "token": User.accessToken,
                          "duration": "month"]
        
        serviceWithToken(headers: headers, parmas: postParams, url: API.KCompareChart, type: CompareChart.self) { (chartData) in
            guard let dates = chartData.dates1, let datesTwo = chartData.dates2, let stocks = chartData.stock1, let stockTwo = chartData.stock2  else{return}
            
            legend = cell.graphView.legend
            legend.form = .circle
            legend.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
            legend.textColor = .white
            legend.horizontalAlignment = .left
            legend.verticalAlignment = .bottom
            legend.orientation = .horizontal
            legend.drawInside = true
            
            xAxis = cell.graphView.xAxis
            xAxis.labelFont = .systemFont(ofSize: 11)
            xAxis.labelTextColor = .lightGray
            xAxis.valueFormatter = DayAxisValueFormatter(chart: cell.graphView)
            
            
            leftAxis = cell.graphView.leftAxis
            leftAxis.labelTextColor = .lightGray
            leftAxis.axisMaximum = 300
            leftAxis.axisMinimum = 0
            leftAxis.drawGridLinesEnabled = false
            leftAxis.granularityEnabled = true
            
            DispatchQueue.main.async {
                for value in 0..<stocks.count{
                    dataEntries.append(ChartDataEntry(x: Double(value), y: stocks[value]))
                }
                
                for value2 in 0..<stockTwo.count{
                    dataTwoEntries.append(ChartDataEntry(x: Double(value2), y: stockTwo[value2]))
                }
                
                let tickerOne = LineChartDataSet(entries: dataEntries, label: self.tickerName)
                tickerOne.setColor(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
                tickerOne.drawCirclesEnabled = false
                tickerOne.drawValuesEnabled = false
                tickerOne.drawFilledEnabled = false
                tickerOne.fillColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
                tickerOne.axisDependency = .left
                tickerOne.lineWidth = 2
                tickerOne.highlightColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                tickerOne.drawCircleHoleEnabled = false
                
                let tickerTwo = LineChartDataSet(entries: dataTwoEntries, label: "VOO")
                tickerTwo.setColor(#colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1))
                tickerTwo.drawCirclesEnabled = false
                tickerTwo.drawValuesEnabled = false
                tickerTwo.drawFilledEnabled = false
                tickerTwo.fillColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                tickerTwo.axisDependency = .left
                tickerTwo.lineWidth = 2
                tickerTwo.drawCircleHoleEnabled = false
                
                let data = LineChartData(dataSets: [tickerOne,tickerTwo])
                data.setValueTextColor(.lightGray)
                data.setValueFont(.systemFont(ofSize: 9))
                cell.graphView.data = data
                cell.graphView.rightAxis.enabled = false
                cell.graphView.animate(xAxisDuration: 2.0)
                self.showMarker(cell: cell)
                
            }
        }
        
    }
    
    // Get Aroon Charts...
    func showAroonChart(cell:GraphsCell){
        
        var legend = Legend()
        var xAxis = XAxis()
        var leftAxis = YAxis()
        var greenDataEntries = [ChartDataEntry]()
        var redDataEntries = [ChartDataEntry]()
        
        let postParams = ["ticker": tickerName ?? "",
                          "token": User.accessToken]
        serviceWithToken(headers: headers, parmas: postParams, url: API.KAroonChart, type: AroonChart.self) { (chartData) in
            guard let status = chartData.status?.code, let message = chartData.status?.message else{return}
            if status == 200{
                guard let aroonData = chartData.data else{return}
                let getGreenValues = aroonData.map({$0.green ?? "0"})
                let getGreen = getGreenValues.map({Double($0)})
                
                for green in 0 ..< getGreenValues.count{
                    greenDataEntries.append(ChartDataEntry(x: Double(green), y: getGreen[green] ?? 0.00))
                }
                
                let getRedValues = aroonData.map({$0.red ?? "0"})
                let getRed = getRedValues.map({Double($0)})
                
                for red in 0 ..< getRed.count{
                    redDataEntries.append(ChartDataEntry(x: Double(red), y: getRed[red] ?? 0.00))
                }
                
                
                DispatchQueue.main.async {
                    legend = cell.graphView.legend
                    legend.form = .circle
                    legend.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
                    legend.textColor = .white
                    legend.horizontalAlignment = .left
                    legend.verticalAlignment = .bottom
                    legend.orientation = .horizontal
                    legend.drawInside = true
                    
                    xAxis = cell.graphView.xAxis
                    xAxis.labelFont = .systemFont(ofSize: 11)
                    xAxis.labelTextColor = .lightGray
                    xAxis.valueFormatter = self
                    
                    leftAxis = cell.graphView.leftAxis
                    leftAxis.labelTextColor = .lightGray
                    leftAxis.axisMaximum = 200
                    leftAxis.axisMinimum = 0
                    leftAxis.drawGridLinesEnabled = false
                    leftAxis.granularityEnabled = true
                    
                    let tickerOne = LineChartDataSet(entries: greenDataEntries, label: "High")
                    tickerOne.setColor(#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
                    tickerOne.drawCirclesEnabled = false
                    tickerOne.drawValuesEnabled = false
                    tickerOne.drawFilledEnabled = false
                    tickerOne.fillColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
                    tickerOne.axisDependency = .left
                    tickerOne.lineWidth = 2
                    tickerOne.highlightColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    tickerOne.drawCircleHoleEnabled = false
                    
                    let tickerTwo = LineChartDataSet(entries: redDataEntries, label: "Low")
                    tickerTwo.setColor(#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                    tickerTwo.drawCirclesEnabled = false
                    tickerTwo.drawValuesEnabled = false
                    tickerTwo.drawFilledEnabled = false
                    tickerTwo.fillColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                    tickerTwo.axisDependency = .left
                    tickerTwo.lineWidth = 2
                    tickerTwo.drawCircleHoleEnabled = false
                    
                    let data = LineChartData(dataSets: [tickerOne,tickerTwo])
                    data.setValueTextColor(.lightGray)
                    data.setValueFont(.systemFont(ofSize: 9))
                    cell.graphView.data = data
                    cell.graphView.rightAxis.enabled = false
                    cell.graphView.animate(xAxisDuration: 1.5)
                    self.showMarker(cell: cell)
                }
            }else{
                self.showWarnig(title: "Aroon Charts", message: message)
            }
        }
    }
    
    // Get Volume & Price Charts...
    func showVolumeChart(cell: GraphsCell){
        
        var volumeDire = [ChartDataEntry]()
        var volumeLin = [ChartDataEntry]()
        
        var legend = Legend()
        var xAxis = XAxis()
        var leftAxis = YAxis()
        
        let postParams = ["ticker": tickerName ?? "",
                          "token": User.accessToken]
        serviceWithToken(headers: headers, parmas: postParams, url: API.KVolumeNPriceChart, type: PriceNVolumeChart.self) { (chartData) in
            guard let volumeD = chartData.volumeList, let volumeL = chartData.volumeLinearList, let priceD = chartData.priceList, let priceL = chartData.priceLinearList else{return}
            for dire in 0..<volumeD.count{
                volumeDire.append(ChartDataEntry(x:Double(dire), y: Double(volumeD[dire][1])))
            }
            for linear in 0..<volumeL.count{
                volumeLin.append(ChartDataEntry(x: Double(linear), y: Double(volumeL[linear][1])))
            }
            
            DispatchQueue.main.async {
                
                legend = cell.graphView.legend
                legend.form = .circle
                legend.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
                legend.textColor = .white
                legend.horizontalAlignment = .left
                legend.verticalAlignment = .bottom
                legend.orientation = .horizontal
                legend.drawInside = true
                
                xAxis = cell.graphView.xAxis
                xAxis.labelFont = .systemFont(ofSize: 11)
                xAxis.labelTextColor = .lightGray
                xAxis.valueFormatter = self
                
                leftAxis = cell.graphView.leftAxis
                leftAxis.labelTextColor = .lightGray
                leftAxis.axisMaximum = 7000000
                leftAxis.axisMinimum = 0
                leftAxis.drawGridLinesEnabled = false
                leftAxis.granularityEnabled = true
                
                let tickerOne = LineChartDataSet(entries: volumeDire, label: "Volume Direction")
                tickerOne.setColor(#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1))
                tickerOne.drawCirclesEnabled = false
                tickerOne.drawValuesEnabled = false
                tickerOne.setDrawHighlightIndicators(true)
                tickerOne.drawFilledEnabled = false
                tickerOne.fillColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
                tickerOne.axisDependency = .left
                tickerOne.lineWidth = 2
                tickerOne.highlightColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                tickerOne.drawCircleHoleEnabled = false
                
                let tickerTwo = LineChartDataSet(entries: volumeLin, label: "Volume Linear")
                tickerTwo.setColor(#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
                tickerTwo.drawCirclesEnabled = false
                tickerTwo.drawValuesEnabled = false
                tickerTwo.drawFilledEnabled = false
                tickerTwo.fillColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                tickerTwo.axisDependency = .left
                tickerTwo.lineWidth = 2
                tickerTwo.drawCircleHoleEnabled = false
                
                let data = LineChartData(dataSets: [tickerOne,tickerTwo])
                data.setValueTextColor(.lightGray)
                data.setValueFont(.systemFont(ofSize: 9))
                cell.graphView.data = data
                cell.graphView.rightAxis.enabled = false
                cell.graphView.animate(xAxisDuration: 3.5)
                self.showMarker(cell: cell)
            }
        }
        
    }
    
    func showPriceChart(cell: GraphsCell){
        
        var priceDire = [ChartDataEntry]()
        var priceLin = [ChartDataEntry]()
        
        var legend = Legend()
        var xAxis = XAxis()
        var leftAxis = YAxis()
        
        let postParams = ["ticker": tickerName ?? "",
                          "token": User.accessToken]
        serviceWithToken(headers: headers, parmas: postParams, url: API.KVolumeNPriceChart, type: PriceNVolumeChart.self) { (chartData) in
            guard let priceD = chartData.priceList, let priceL = chartData.priceLinearList else{return}
            
            for dire in 0..<priceD.count{
                priceDire.append(ChartDataEntry(x: Double(dire), y: Double(priceD[dire][1])))
                
            }
            for linear in 0..<priceL.count{
                priceLin.append(ChartDataEntry(x: Double(linear), y: Double(priceL[linear][1])))
            }
            
            DispatchQueue.main.async {
                legend = cell.graphView.legend
                legend.form = .circle
                legend.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
                legend.textColor = .white
                legend.horizontalAlignment = .left
                legend.verticalAlignment = .bottom
                legend.orientation = .horizontal
                legend.drawInside = true
                
                xAxis = cell.graphView.xAxis
                xAxis.labelFont = .systemFont(ofSize: 11)
                xAxis.labelTextColor = .lightGray
                xAxis.valueFormatter = self
                
                leftAxis = cell.graphView.leftAxis
                leftAxis.labelTextColor = .lightGray
                leftAxis.axisMaximum = 500
                leftAxis.axisMinimum = 0
                leftAxis.drawGridLinesEnabled = false
                leftAxis.granularityEnabled = true
                
                let tickerOne = LineChartDataSet(entries: priceDire, label: "Price Direction")
                tickerOne.setColor(#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                tickerOne.drawCirclesEnabled = false
                tickerOne.drawValuesEnabled = false
                tickerOne.setDrawHighlightIndicators(true)
                tickerOne.drawFilledEnabled = false
                tickerOne.fillColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
                tickerOne.axisDependency = .left
                tickerOne.lineWidth = 2
                tickerOne.highlightColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                tickerOne.drawCircleHoleEnabled = false
                
                let tickerTwo = LineChartDataSet(entries: priceLin, label: "Price Linear")
                tickerTwo.setColor(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
                tickerTwo.drawCirclesEnabled = false
                tickerTwo.drawValuesEnabled = false
                tickerTwo.drawFilledEnabled = false
                tickerTwo.fillColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                tickerTwo.axisDependency = .left
                tickerTwo.lineWidth = 2
                tickerTwo.drawCircleHoleEnabled = false
                
                let data = LineChartData(dataSets: [tickerOne,tickerTwo])
                data.setValueTextColor(.lightGray)
                data.setValueFont(.systemFont(ofSize: 9))
                cell.graphView.data = data
                cell.graphView.rightAxis.enabled = false
                cell.graphView.animate(xAxisDuration: 2.5)
                cell.graphView.drawMarkers = true
                self.showMarker(cell: cell)
            }
        }
        
    }
    
    func showMarker(cell: GraphsCell){
        let marker = BalloonMarker(color: .black,
                                   font: .systemFont(ofSize: 10),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 4, left: 4, bottom: 10, right: 4))
        marker.chartView = cell.graphView
        marker.minimumSize = CGSize(width: 40, height: 25)
        cell.graphView.marker = marker
    }
}

