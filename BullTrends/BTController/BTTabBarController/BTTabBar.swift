//
//  BTTabBar.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 02/01/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

@IBDesignable
class BTTabBar: UITabBar {
    private var shapeLayer: CALayer?
    
    override func draw(_ rect: CGRect) {
        self.addShape()
    }
    
    private func addShape(){
      let shapeLayer = CAShapeLayer()
        shapeLayer.path = createPathCircle()
        shapeLayer.fillColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        shapeLayer.shadowOffset = CGSize(width: 0, height: -10)
        shapeLayer.shadowOpacity = 0.07
        shapeLayer.shadowRadius = 8
        shapeLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        if let oldShapeLayer = self.shapeLayer{
            self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        }else{
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
        self.shapeLayer = shapeLayer
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let buttonRadius: CGFloat = 40
        return abs(self.center.x - point.x) > buttonRadius || abs(point.y) > buttonRadius
    }
    
    func createPathCircle() -> CGPath{
        let radius: CGFloat = 37.0
        let path = UIBezierPath()
        let centerWidth = self.frame.width/2
        
        path.move(to: CGPoint(x:0, y: 0))
        path.addLine(to: CGPoint(x: (centerWidth - radius * 2), y: 0))
        path.addArc(withCenter: CGPoint(x: centerWidth, y: 0), radius: radius, startAngle: CGFloat(180).degreesToRadians, endAngle: CGFloat(0).radiansToDegrees, clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()
        return path.cgPath
    }
}

extension CGFloat{
    var degreesToRadians: CGFloat{return self * .pi / 180}
    var radiansToDegrees: CGFloat{return self * 180 / .pi}
}
