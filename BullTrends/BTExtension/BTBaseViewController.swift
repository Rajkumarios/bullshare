//
//  BTBaseViewController.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 08/12/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import UIKit

class BTBaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Hide navigation...
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // Navigate to back view...
    @IBAction func popToViewController(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
     func popToSelectedVc(_ vc: UIViewController){
        self.navigationController?.popToViewController(vc, animated: true)
    }
    
    //Navigate to next view controller...
    func navigateTo(viewController: UIViewController){
        self.navigationController?.pushViewController(viewController, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
}


extension UIViewController{
    
    //Present view controller...
    func presentVCModally(storyboard: String){
        DispatchQueue.main.async {
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: storyboard)else{return}
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .overCurrentContext
            nav.modalTransitionStyle = .crossDissolve
            self.tabBarController?.tabBar.isHidden = true
            self.present(nav, animated: true, completion: nil)
            
        }
    }
    

    // Present view controller with navigation...
    func presentVcWithNavigation(vc: UIViewController){
        DispatchQueue.main.async {
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    // Dismiss viewcontroller...
    func dismissVC(){
        dismiss(animated: true, completion: nil)
    }
}
