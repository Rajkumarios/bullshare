//
//  GraphsCell.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 12/04/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit
import Charts

class GraphsCell: UICollectionViewCell {
    @IBOutlet weak var graphView: LineChartView!
  
}
