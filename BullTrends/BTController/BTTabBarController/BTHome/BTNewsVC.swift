//
//  BTNewsVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 10/03/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTNewsVC: BTBaseViewController, XMLParserDelegate {
    
    // Variables Declaration...
    private let news_cell_id = "NewsCell"
    var tickerName: String?
    var newsArr = TickerNews()
    
    var elementName: String = String()
    var newsTitle = String()
    var newsDescription = String()
    var newsLink = String()
    var newsDate = String()
    
    @IBOutlet weak var newsTV: UITableView!
    
    // Life-Cycles...
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        parseBTNews()
        getNewsData()

    }
    
    // News Data...
    func getNewsData(){
        
        guard let ticker = tickerName else {return}
        let servieURL = "https://api.iextrading.com/1.0/stock/\(ticker)/news"
        getServiceData(serviceName: servieURL, type: TickerNews.self) { (newsData) in
            self.newsArr = newsData
            DispatchQueue.main.async {
                self.newsTV.reloadData()
            }
        }
    }
    
    func parseBTNews(){
        guard let ticker = tickerName else {return}
        let service = "http://wwwqa.bull-trends.com/rss/stock/\(ticker).rss"
        let serviceURL = NSURL(string: service)
        //Creating data task
        let task = URLSession.shared.dataTask(with: serviceURL! as URL) { (data, response, error) in
            
            if data == nil {
                print("dataTaskWithRequest error: \(String(describing: error?.localizedDescription))")
                return
            }
            
            let parser = XMLParser(data: data!)
            parser.delegate = self
            parser.parse()
            
        }
        
        task.resume()
    }
}

// Tableview Delegate and Datasources..
extension BTNewsVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: news_cell_id, for: indexPath) as! NewsCell
        let news = newsArr[indexPath.row]
        cell.newTitleLbl.text = news.headline
        cell.newsDetailLbl.text = news.summary
        cell.newsDateLbl.text = news.datetime
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsDetailRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTNewsDetailVc) as! BTNewsDetailPage
        newsDetailRef.newsDetailURL = newsArr[indexPath.row].url
        self.navigateTo(viewController: newsDetailRef)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // 1
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        if elementName == "item" {
            newsTitle = String()
            newsDescription = String()
            newsLink = String()
            newsDate = String()

        }
        
        self.elementName = elementName
    }
    
    // 2
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            let btNews = News(datetime: newsDate, headline: newsTitle, source: "", url: newsLink, summary: "", related: "", image: "")
            newsArr.append(btNews)
            print(newsArr)
            
            DispatchQueue.main.async {
                self.newsTV.reloadData()
            }
        }
    }
    
    // 3
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if (!data.isEmpty) {
            if self.elementName == "title" {
                newsTitle += data
            } else if self.elementName == "description" {
                newsDescription += data
            }else if self.elementName == "link"{
                newsLink += data
            }else if self.elementName == "pubDate"{
                newsDate += data
            }
        }
    }
    
}
