//
//  BTOnboardCell.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 20/11/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import UIKit

class BTOnboardCell: UICollectionViewCell {
    @IBOutlet weak var pageImg: UIImageView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var pageSubtitle: UILabel!

}
