//
//  BTCreateNewPasswordVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 01/06/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTCreateNewPasswordVC: BTBaseViewController {

    var getOTPFromUser: String?
    var warningMessageStr = String()
    @IBOutlet weak var newPassowrdTF: UITextField!
    @IBOutlet weak var confirmPasswordTf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onUpdatePasswordBtnTapped(_ sender: UIButton) {
        warningMessageStr = ""
        checkPasswords()
    }
    
    func checkPasswords(){
        if newPassowrdTF.text == confirmPasswordTf.text{
            createNewPassword()
        }else{
            showWarnig(title: "Create Password", message: "Please confirm a valid passowrd")
        }
    }
    
    func createNewPassword(){
        let user_id = UserDefaults.standard.value(forKey: "user_forgot") as! String
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let parameters = [
            "password": confirmPasswordTf.text ?? "Bull@123",
            "confirmCode": getOTPFromUser ?? "000000",
            "userid": user_id ] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: parameters, url: API.kPassword, type: ChangePassword.self) { (pswdChangeRequest) in
            guard let statusCode = pswdChangeRequest.statusCode else{return}
            
            if statusCode == 200{
                DispatchQueue.main.async {
                    let loginVcRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTLoginVc) as! BTLoginVC
                    self.navigateTo(viewController: loginVcRef)
                }
            }else{
                if let errorList = pswdChangeRequest.errorList{
                    for i in 0 ..< errorList.count{
                        self.warningMessageStr.append("\(errorList[i])\n")
                    }
                    DispatchQueue.main.async {
                        self.showWarnig(title: "OTP Verification", message: self.warningMessageStr)
                    }
                }
            }
            
        }
    }
}
