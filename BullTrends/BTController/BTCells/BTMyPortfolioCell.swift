//
//  BTMyPortfolioCell.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 15/05/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTMyPortfolioCell: UICollectionViewCell {
    @IBOutlet weak var portfolioName: UILabel!
    @IBOutlet weak var gainLbl: UILabel!
    @IBOutlet weak var portfolioBGV: BTViewCorners!
    
}
