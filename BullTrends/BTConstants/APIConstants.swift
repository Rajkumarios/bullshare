//
//  APIConstants.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 28/11/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import Foundation

//falcon-fit3
enum API{
    static let LBaseURL             = "https://wwwqa.bull-trends.com/login/"
    
    static let kLogin               = LBaseURL+"oauth/token"
    static let kRegistration        = LBaseURL+"mu/register"
    static let kPasswordChange      = LBaseURL+"mu/passwordChange"
    static let kPassword            = LBaseURL+"mu/password"
    
    
    static let kBaseURL             = "https://wwwqa.bull-trends.com/BullTrends/"
  
// ETF...
    static let kETF                 = kBaseURL+"mu/stock/etf"
    static let KDelayed             = kBaseURL+"mu/stock/delayed"
    static let KGain                = kBaseURL+"mu/stock/gain"

//Charts..
    static let KCompareChart        = kBaseURL+"mu/stock/compare"
    static let KAroonChart          = kBaseURL+"mu/stock/aroon"
    static let KVolumeNPriceChart   = kBaseURL+"mu/stock/volume"

//Search...
    static let KTrendSearch         = kBaseURL+"mu/stock/trend"
    
    static let kPortfolioList       = kBaseURL+"mu/portfolio/list"
    static let kPortfolioDetail     = kBaseURL+"mu/portfolio/detail"
    static let kPortfolioChart      = kBaseURL+"mu/portfolio/chart"
    static let kPortfolioDelete     = kBaseURL+"mu/portfolio/rm"
    static let kPortfolioPrice      = kBaseURL+"mu/stock/closeondate"
    static let kTickerSearch        = kBaseURL+"mu/stock/tickersearch"
    static let kTickerSave          = kBaseURL+"mu/portfolio/save"
    static let kNewStockboard       = kBaseURL+"mu/msg/mbforum"
    static let kForumsList          = kBaseURL+"mu/msg/mbforum"
    static let kReplyList           = kBaseURL+"mu/msg/replylist"
    static let kPostsList           = kBaseURL+"mu/msg/postlist"
    static let kPrivateForum        = kBaseURL+"mu/msg/mbprivateforum"
    static let kPrivateForumMembers = kBaseURL+"mu/msg/mbprivateforummembers"
    static let kPostCreation        = kBaseURL+"mu/msg/mbpost"
    static let kUserRole            = kBaseURL+"mu/msg/mbuser"
    static let kMsgLike             = kBaseURL+"mu/msg/like"
    static let kAddMember           = kBaseURL+"mu/msg/mbprivateforum"
    static let kSearchMembers       = kBaseURL+"mu/msg/mbalias"
}


// Token Types
enum Token{
   static let kAccessToken      = "accessToken"
   static let kRefreshToken     = "refreshToken"
   static let kInactiveTimeout  = "appInactiveTimeout"
}

//Header keys
enum Headers{
    static let authorization    = "Authorization"
    static let deviceUniqueId   = "DeviceUniqueId"
    static let nonce            = "Nonce"
    static let hash             = "Hash"
    static let contentType      = "Content-Type"
    static let accept           = "Accept"
}

// Admin Login...
enum Admin{
    static let userName         =   "mobile_app_user"
    static let password         =   "CF6FB356A8982F7930BA38C5AF12A9D0A6D702228A18B18B775D4DB4F142BE2F"
    static let grantType        =   "password"
    static let scope            =   "write"
    static let clientSecret     =   "AFFE3DB59C7E90EE492F5D83FFCD98931E26C9FE47631DB3B8A737DB6C99EE7E"
    static let clientID         =   "bullTrendsAdmin"
}

//Content-Type
enum ContentType{
    static let X_www_form     = "application/x-www-form-urlencoded"
    static let Json           = "application/json"
}

// Authorization Tokens

enum Authorization{
    static let Login          = "Basic YnVsbFRyZW5kc0xvZ2luUUE6QUU3ODQ4QkE3MDgxNkU0Q0Y0OERERDFBNTU4MzMzNURGMUY3MEU5M0Y4MDgyMDZERUE2MDI3MDgwOUU1Qjg0Ng=="
    static let AdminLogin     = " Basic YnVsbFRyZW5kc0FkbWluOkFGRkUzREI1OUM3RTkwRUU0OTJGNUQ4M0ZGQ0Q5ODkzMUUyNkM5RkU0NzYzMURCM0I4QTczN0RCNkM5OUVFN0U="
    static let Registration   = "Bearer \(UserDefaults.standard.value(forKey: "admin_access")as? String ?? "")"
    
}

// Secret IDs
enum SecretID{
    static let Client_Secret  = "AE7848BA70816E4CF48DDD1A5583335DF1F70E93F808206DEA60270809E5B846"
    static let Client_Id      = "bullTrendsLoginQA"
}




//View controlers Storyboard IDs
enum StoryboardId{
    static let BTNavigation             = "RootNavigation"
    static let BTInternetVc             = "ConnectionLostVc"
    static let BTLoginVc                = "BTLoginVC"
    static let BTRegistrationVc         = "BTRegistrationVC"
    static let BTCountrySelection       = "BTCountrySelectionVC"
    static let BTForgotPasswordVc       = "BTForgotPasswordVC"
    static let BTOTPVc                  = "BTOTPVC"
    static let BTCreateNewPasswordVC    = "BTCreateNewPasswordVC"
    static let BTTermsAndConditionsVc   = "BTTermsAndConditions"
    static let BTTabbarVc               = "BTTabBarController"
    
    static let BTHomeVc                 = "BTHomeVC"
    static let BTStatisticsVc           = "ETFStatisticsVC"
    static let BTAdvancedSearchVc       = "BTAdvancedSearchVc"
    static let BTNewsVc                 = "BTNewsVC"
    static let BTNewsDetailVc           = "BTNewsDetailPage"
    
    static let BTMoreVC                 = "BTMoreVC"

    static let BTPortfolioVc            = "BTPortfolioVC"
    static let BTAddNewPortfolioVc      = "BTAddNewPortfolioVC"
    
}
