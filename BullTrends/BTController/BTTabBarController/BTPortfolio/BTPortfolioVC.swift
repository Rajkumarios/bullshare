//
//  BTPortfolioVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 04/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTPortfolioVC: BTBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onAddNewPortfolioBtnTapped(_ sender: UIButton) {
        let addNewPortfolioRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTAddNewPortfolioVc) as! BTAddNewPortfolioVC
        self.navigateTo(viewController: addNewPortfolioRef)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
