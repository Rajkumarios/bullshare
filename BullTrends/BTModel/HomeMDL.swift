//
//  HomeMDL.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 05/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import Foundation

// ETF MDL...
 struct Etf: Codable {
 let status: Status?
 let data: [ETFData]?
}

struct Status: Codable {
    let code: Int?
    let message: String?
}

 struct ETFData: Codable {
    let ticker: String?
    let trend: String?
    let stars, price: String?
    let dividendYield: String?
    let name: String?
    let exchange: String?
    let category: String?
    let netChange, percentChange: Double?
    let volume, dt: Int?
    let country: String?
}

//enum Exchange: String, Codable {
//    case nasdaq = "NASDAQ"
//    case nyse = "NYSE"
//}
//
//enum Trend: String, Codable {
//    case bear = "BEAR"
//    case bull = "BULL"
//}

// My portfolio MDL...

struct UserPortfolio: Codable {
    let status: Status
    let data: [MyPortfolio]
}

struct MyPortfolio: Codable {
    let id, portfolioName: String
//    let stockList: [StockList]
//    let startDate, createdDate, modifiedDate: Int
//    let countryCd: String
//    let dt: Int
//    let token: String
}

struct StockList: Codable {
    let ticker: String
    let price: Double
    let shares: Int
    let gainPct, value: Double
    let pct: Int
}

// Statistics MDL...
struct Statistics: Codable {
    let status: Status?
    let data: [Stats]?
}

struct Stats: Codable {
    let ticker, name, last, netChange: String?
    let percentChange, volume, tradeTime, exchange: String?
    let dt: Int?
    let open, high, low: String?
    let stkCntry, etf: String?
    let dailyInd, week52Low, week52High: String?
}

//Gain MDL...
struct Gain: Codable {
    let status: Status?
    let data: [GainData]?
}
struct GainData: Codable {
    let ytdGain,yearGain,day30Gain,dividend: Double?
}


// Comapare Chart MDL...
struct CompareChart: Codable {
    let ticker1, ticker2: String?
    let stock1,stock2: [Double]?
    let dates1, dates2: [Int]?
}

// Aroon Chart MDL...
struct AroonChart: Codable {
    let status: Status?
    let data: [AroonData]?
}

struct AroonData: Codable {
    let green, red, green50, red50: String?
    let green25, red25: String?
    let dt: Int?
    let ticker: String?
}

// Price and volume Charts...

struct PriceNVolumeChart: Codable {
    let trend, days, avgVolume, minVolume: String?
    let maxVolume, status: String?
    let volumeList, volumeLinearList: [[Int]]?
    let priceList: [[Double]]?
    let priceLinearList: [[Int]]?
}

// F - score...


// News...

typealias TickerNews = [News]
struct News: Codable {
    let datetime, headline, source, url, summary, related, image: String?
}
