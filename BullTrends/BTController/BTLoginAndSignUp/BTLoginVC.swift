//
//  BTLoginVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 29/11/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class BTLoginVC: BTBaseViewController,UITextFieldDelegate {
    
// Variables Delaration
    var warningMessageStr: String = ""
// IBOutlets Declaration..
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
// View life- clycles...
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
// Update UI related elements...
    func updateUI(){
        userNameTF.delegate = self
        passwordTF.delegate = self
        userNameTF.textContentType = .username
        passwordTF.textContentType = .password
    }
    
// Textfield Delegate methods calling...
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNameTF{
            passwordTF.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
// IBActions...
    @IBAction func onForgotBtnTapped(_ sender: UIButton) {
        let forgotpswdRef = storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTForgotPasswordVc)
        navigateTo(viewController: forgotpswdRef!)
    }
    
// On proceed Button tapped..we have to check given credentials right or not...
    @IBAction func onProceedBtnTapped(_ sender: BTButton) {
        AdminLogin()
    }
    
// MARK: Login-service calling...
    func loginWith(userName: String, password: String){
        
        let headers: [String: String] = [Headers.authorization: Authorization.Login,
                                         Headers.contentType: ContentType.X_www_form]
        
        let posString = "username=\(userName)" + "&password=\(password)" + "&grant_type=password" + "&scope=read" + "&client_secre=\(SecretID.Client_Secret)" + "&client_id=\(SecretID.Client_Id)"
        
        postServiceCallWith(header: headers, serviceURL: API.kLogin, params: posString, type: BTLogin.self) { (user) in
            
            if user.accessToken == nil{
                 DispatchQueue.main.async {
                    self.showWarnig(title: Message.title, message: "Please Enter valid Details...😯")
                }
            }else{
                defaults.set(true, forKey: "status")
                defaults.set(user.accessToken, forKey: "access_token")
                defaults.set(user.emailAddress, forKey: "emailAddress")
//                defaults.set(user.phoneNumber, forKey: "phoneNumber")
                defaults.set (user.expiresIn, forKey: "user_expire_date")
                defaults.set(user.fullName, forKey: "fullName")

                DispatchQueue.main.async {
                    let TabbarControllerRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTTabbarVc)
                    self.navigateTo(viewController: TabbarControllerRef!)
                }
            }
        }
    }
    
    @IBAction func facebookBtnTapped(_ sender: BTButtonCorners) {
        loginWitFacebook()
    }
    
// MARK: Facebook Sign-in integration...
    func loginWitFacebook(){
        let FBManager = FBSDKLoginManager()
        FBManager.logIn(withReadPermissions: ["public_profile", "email","user_birthday"], from: self) { (result, error) in
            if error != nil{
                print(error!.localizedDescription)
            }else if result!.isCancelled{
                print("user Cancelled login")
            }else{
                self.fetchUserProfile()
            }
        }
    }
    
// MARK:  get user-info from Facebook
    func fetchUserProfile(){
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id, email, birthday, name, picture.width(480).height(480)"]).start { (connection, result, error) in
            if error != nil{
                print("error from getting user details: \(error!)")
            }else{
                print(result!)
                
                let defaults = UserDefaults.standard
                let userInfo = result as AnyObject
//                let id: String = userInfo.value(forKey: "id") as! String
                let name = userInfo.value(forKey: "name") as! String
                let email = userInfo.value(forKey: "email") as! String
               
                defaults.set("true", forKey: "status")
                defaults.set(name, forKey: "fullName")
                defaults.set(email, forKey: "emailAddress")

                if let profilePictureObj = userInfo.value(forKey:"picture") as? NSDictionary{
                    let data = profilePictureObj.value(forKey: "data") as! NSDictionary
                    let pictureUrlString  = data.value(forKey:"url") as! String
                    defaults.set(pictureUrlString, forKey: "image_url")
                }
                guard let country = UserDefaults.standard.value(forKey: "country") as? String else{return}
                var mobileNo = ""
                switch country {
                case "IN":
                    mobileNo = "+91\(mobileNo)"
                default:
                    mobileNo = "+1\(mobileNo)"
                }
                self.userRegistration(fullName: name, nameAlias: name, email: email, mobile: mobileNo, password: "Bull@123")
                DispatchQueue.main.async {
                    let TabbarControllerRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTTabbarVc)
                    self.navigateTo(viewController: TabbarControllerRef!)
                }
            }
        }
    }
    
    @IBAction func registationBtnTapped(_ sender: UIButton) {
        let registrationRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTRegistrationVc)
        navigateTo(viewController: registrationRef!)
    }
    
// MARK: Admin login for access-token...
    func AdminLogin(){
        let headers: [String: String] = [Headers.authorization: Authorization.AdminLogin,
                                         Headers.contentType: ContentType.X_www_form]
        
         let posString = "username=\(Admin.userName)" + "&password=\(Admin.password)" + "&grant_type=\(Admin.grantType)" + "&scope=\(Admin.scope)" + "&client_secre=\(Admin.clientSecret)" + "&client_id=\(Admin.clientID)"
        
        postServiceCallWith(header: headers, serviceURL: API.kLogin, params: posString, type: BTLogin.self) { (admin) in
            if admin.accessToken == nil{
                DispatchQueue.main.async {
                    self.showWarnig(title: Message.title, message: "Please Enter valid Details...😯")
                }
            }else{
                defaults.set(true, forKey: "status")
                defaults.set(admin.accessToken, forKey: "admin_access")
                DispatchQueue.main.async {
                    guard let name = self.userNameTF.text, let password = self.passwordTF.text else{return}
                    if isValidEmail(testStr: name) == false || isValidPassword(testStr: password) == false{
                        Message.title = "Login"
                        Message.message = "Please enter valid details"
                        self.showWarnig(title: Message.title, message: Message.message)
                    }else{
                        self.loginWith(userName: name, password: password)
                    }
                }
            }
        }

    }
}

extension BTLoginVC{
    
    func userRegistration(fullName: String, nameAlias: String, email: String, mobile: String, password: String){
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let parameters = [
            "email": email,
            "fullName": fullName,
            "mbAlias": nameAlias,
            "password": password,
            "phoneNumber": mobile] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: parameters, url: API.kRegistration, type: BTRegistration.self) { (registration) in
            guard let statusCode = registration.statusCode else{return}
            
            if statusCode == 403{
                if let errorList = registration.errorList{
                    for i in 0 ..< errorList.count{
                        self.warningMessageStr.append("\(errorList[i])\n")
                    }
                    self.showWarnig(title: "Registration", message: self.warningMessageStr)
                }
            }else{
                let countryVcRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTCountrySelection)
                self.navigateTo(viewController:countryVcRef!)
            }
        }
    }

}
