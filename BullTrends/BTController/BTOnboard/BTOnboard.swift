//
//  ViewController.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 17/11/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import UIKit

class BTOnboard: BTBaseViewController {
     struct Onboard{
        let image: UIImage
        let title: String
        let description: String
    }
    
    var onboardPages = [Onboard]()
    fileprivate let cellID = "onboardCell"
    
    @IBOutlet weak var BTOnboardCV: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        AdminLogin()
    }
    
    func updateUI(){
        pageControl.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        let first = Onboard(image: UIImage(named: "1.png")!, title: "Intro", description: "Fairness.Transparency.Reliable.Secured.High quality analytical Data.No Fees")
        
        let second = Onboard(image: UIImage(named: "1.png")!, title: "Trends", description: "Unlock trends, patterns, scores to make strategic and wise decisions")
       
        let third = Onboard(image: UIImage(named: "1.png")!, title: "Portfolio & News", description: "Create and play with your own virtual portfolios to get insights and educative information to make desicions.\n Follow Bulltrends proprietary news")
     
        let forth = Onboard(image: UIImage(named: "1.png")!, title: "Stock Boards", description: "Experience the first of its kind Stock boards.\n Surf public boards and build trading network.\n Create own private boards for communication, storing information and images. \n Information is knowledge and wealth. Stay connected with world of trading experts to share information on markets")
        onboardPages = [first,second,third,forth]
        
    }
    @IBAction func onSkipBtnTapped(_ sender: UIButton) {
        let loginRef = storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTLoginVc)
        navigateTo(viewController: loginRef!)
        
    }
}

// MARK: UICollection ViewDataSource

extension BTOnboard: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        pageControl.numberOfPages = onboardPages.count
        return onboardPages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID  , for: indexPath) as! BTOnboardCell
        cell.pageImg.image = onboardPages[indexPath.row].image
        cell.pageTitle.text = onboardPages[indexPath.row].title
        cell.pageSubtitle.text = onboardPages[indexPath.row].description
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: BTOnboardCV.frame.width, height: BTOnboardCV.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        
        if let currentPage = BTOnboardCV.indexPathForItem(at: center) {
            pageControl.currentPage = currentPage.row
        }
    }
    
}
//MARK :
extension BTOnboard{
    func AdminLogin(){
        let headers: [String: String] = [Headers.authorization: Authorization.AdminLogin,
                                         Headers.contentType: ContentType.X_www_form]
        
        let posString = "username=\(Admin.userName)" + "&password=\(Admin.password)" + "&grant_type=\(Admin.grantType)" + "&scope=\(Admin.scope)" + "&client_secre=\(Admin.clientSecret)" + "&client_id=\(Admin.clientID)"
        
        postServiceCallWith(header: headers, serviceURL: API.kLogin, params: posString, type: BTLogin.self) { (admin) in
            if admin.accessToken == nil{
                DispatchQueue.main.async {
                    self.showWarnig(title: Message.title, message: "Please Enter valid Details...😯")
                }
            }else{
                defaults.set(admin.accessToken, forKey: "admin_access")
                
            }
        }
        
    }

}

