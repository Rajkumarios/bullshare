//
//  Defaults.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 08/12/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import Foundation

//User Information...
    let defaults = UserDefaults.standard

enum User{
    
    static let fullName         =   defaults.value(forKey: "fullName") as? String
    static let phoneNumber      =   defaults.value(forKey: "phoneNumber") as? String
    static let emailAddress     =   defaults.value(forKey: "emailAddress") as? String
    static let userIdentifier   =   defaults.value(forKey: "userIdentifier") as? String
    static let accessToken      =   defaults.value(forKey: "access_token") as? String ?? "Access token not found"
    static let userCountry      =   defaults.value(forKey: "country") as? String ?? " "
}

//Show warning title and message....
enum Message{
    static var title = ""
    static var message = ""
}
