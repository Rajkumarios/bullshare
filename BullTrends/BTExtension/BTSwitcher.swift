
import Foundation
import UIKit

class Switcher {
    static func updateRootVC(){
        
        let status = UserDefaults.standard.bool(forKey: "status")
        var rootVC : UIViewController?
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        print(status)
        
        if status{
            let TabbarControllerRef = storyboard.instantiateViewController(withIdentifier: StoryboardId.BTTabbarVc)
            rootVC = TabbarControllerRef
        }else{
            rootVC = storyboard.instantiateViewController(withIdentifier: StoryboardId.BTNavigation)
        }
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = rootVC
        }
    }
}
