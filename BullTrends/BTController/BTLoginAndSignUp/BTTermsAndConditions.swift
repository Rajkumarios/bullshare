//
//  BTTermsAndConditions.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 30/01/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTTermsAndConditions: BTBaseViewController {

    @IBOutlet weak var TermsNcTV: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func acceptCloseBtnTapped(_ sender: UIButton) {
        dismissVC()
    }
}
