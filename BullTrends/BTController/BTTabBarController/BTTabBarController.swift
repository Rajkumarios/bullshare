//
//  BTTabBarController.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 02/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTTabBarController: UITabBarController,UITabBarControllerDelegate{

//    let button = UIButton.init(type: .custom)

    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
//        setMiddleTab()
    }
    
    func updateUI(){
//       self.delegate = self
        dropShadowForTabbar()
    }
    
    func dropShadowForTabbar(){
        tabBar.layer.shadowOffset = CGSize(width: 0, height: -10)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.1
    }
    
//    func setMiddleTab(){
//        button.backgroundColor = .white
//        let tintedImage = UIImage(named: "stock")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
//        button.setImage(tintedImage, for: .normal)
//        button.addTarget(self, action: #selector(stockBtnTapped), for: .touchUpInside)
//        button.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//
//        button.layer.cornerRadius = 32
//        button.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        button.layer.shadowOffset = CGSize(width: 0, height: 0)
//        button.layer.shadowRadius = 5
//        button.layer.shadowOpacity = 0.1
//        self.view.insertSubview(button, aboveSubview: self.tabBar)
//    }
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        // safe place to set the frame of button manually
//        button.frame = CGRect.init(x: self.tabBar.center.x - 32, y: self.view.bounds.height - 80, width: 64, height: 64)
//    }
//    @objc func stockBtnTapped(){
//        button.tintColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
//        self.tabBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//    }
//
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        self.tabBar.tintColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
//        let index = tabBarController.selectedIndex
//        switch index {
//        case 0...4:
//            button.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        default:
//            button.tintColor = #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1)
//        }
//    }
}
