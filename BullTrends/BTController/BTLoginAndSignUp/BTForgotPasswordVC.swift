//
//  BTForgotPasswordVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 07/01/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTForgotPasswordVC: BTBaseViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    
    var warningMessageStr: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func sendOTPBtnTapped(_ sender: Any) {
        warningMessageStr = ""
        passwordChangeRequest()
    }
    
    // Password Change Request...
    func passwordChangeRequest(){
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let parameters = [
            "sendSms": true,
            "sendEmail": true,
            "confirmSMS": true,
            "userid": emailTF.text!] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: parameters, url: API.kPasswordChange, type: ChangePassword.self) { (pswdChangeRequest) in
            guard let statusCode = pswdChangeRequest.statusCode else{return}
            
            if statusCode == 403{
                if let errorList = pswdChangeRequest.errorList{
                    for i in 0 ..< errorList.count{
                        self.warningMessageStr.append("\(errorList[i])\n")
                    }
                    DispatchQueue.main.async {
                        self.showWarnig(title: "Password Change", message: self.warningMessageStr)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    UserDefaults.standard.setValue(self.emailTF.text!, forKey: "user_forgot")
                    let OTPVcRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTOTPVc) as! BTOTPVC
                        self.navigateTo(viewController:OTPVcRef)
                }
            }
            
        }
    }
}
