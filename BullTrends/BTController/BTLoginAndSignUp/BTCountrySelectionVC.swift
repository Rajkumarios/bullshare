//
//  BTCountrySelectionVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 26/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTCountrySelectionVC: BTBaseViewController {
    
    @IBOutlet weak var indiaVw: UIView!
    @IBOutlet weak var indiaBtn: UIButton!
    @IBOutlet weak var usaBtn: UIButton!
    @IBOutlet weak var usaVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func countrySelectionbtnTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            usaVw.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            indiaVw.layer.borderColor = #colorLiteral(red: 0.2790801227, green: 0.6332188845, blue: 0.4609255791, alpha: 1)
            indiaVw.layer.borderWidth = 3
            defaults.set("IN", forKey: "country")
            gotoHomeVc()
        default:
            indiaVw.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            usaVw.layer.borderColor = #colorLiteral(red: 0.2790801227, green: 0.6332188845, blue: 0.4609255791, alpha: 1)
            usaVw.layer.borderWidth = 3
            defaults.set("USA", forKey: "country")
            gotoHomeVc()
        }
    }
    
    // Navigate to Home Page after successfully registered and selected Country...
    func gotoHomeVc(){
        let TabbarControllerRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTTabbarVc)
        navigateTo(viewController: TabbarControllerRef!)
        showWarnig(title: Message.title, message: "You've Successfully Logged in. 😃 😃")
    }
    
}
