//
//  BTHomeVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 04/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTHomeVC: BTBaseViewController {
    
    // Variable Declaration...
    var etfArr = [ETFData]()
    var myPortfolioArr = [MyPortfolio]()
    private let ETF_Cell_Id = "ETFCell"
    private let portfolio_cell_id = "BTMyPortfolioCell"
    private let country = UserDefaults.standard.value(forKey: "country") as? String
    private let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                     Headers.contentType: ContentType.Json]

    // IBOutlets Declaration...
    @IBOutlet weak var myPortfolioCV: UICollectionView!
    @IBOutlet weak var etfTV: UITableView!
    @IBOutlet weak var userNameLbl: UILabel!
    
    // Life-Cycles...
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        myPortfolioServiceCall()
        getEtfData()
    }
    
    // update UI...
    func updateUI(){
        guard let expireDate = UserDefaults.standard.value(forKey: "user_expire_date") as? Int else{return}
        if converMillsecToDate(expireDate) < 1{
            defaults.set(false, forKey: "status")
            Switcher.updateRootVC()
        }else{
            userNameLbl.text = User.fullName
        }
    }
    
    //MARK: ETF  AND PORTFOLIO SERVICES CALLING...
   
    func getEtfData(){
        
        //after successfully logged in.. we can get User access-token then only we can use User.accessToken otherwise in userdefaults nil value placed..
            let postParams =
            ["country": "USA", // show current country here...
             "token": User.accessToken]  // "token": User.accessToken
        
        serviceWithToken(headers: headers, parmas: postParams as [String : Any], url: API.kETF, type: Etf.self) { (etfData) in
            print(etfData.data as Any)
            guard let status = etfData.status?.code , let message = etfData.status?.message else{return}
            if status == 555{
                self.showWarnig(title: "Data Loading..", message: message)
            }else{
                if let etfData = etfData.data{
                    self.etfArr = etfData
                    DispatchQueue.main.async {
                        self.etfTV.reloadData()
                    }
                }else{
                    return
                }
            }
        }
    }
    
    func myPortfolioServiceCall(){
        let postParams =
            ["countryCd": "USA", // show current country here...
                "token": User.accessToken]  // "token": User.accessToken
        
        serviceWithToken(headers: headers, parmas:postParams , url: API.kPortfolioList, type: UserPortfolio.self) { (portfolio) in
            guard let status = portfolio.status.code, let message = portfolio.status.message else{return}
            
            if status == 200{
                 self.myPortfolioArr = portfolio.data
                
                DispatchQueue.main.async {
                    self.myPortfolioCV.reloadData()
                }
            }else{
                self.showWarnig(title: "Portfolio", message: message)
            }
        }
    }
    
    @IBAction func alram(_ sender: Any) {
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
}

extension BTHomeVC: UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- TableView Data source methods..
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return etfArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = etfTV.dequeueReusableCell(withIdentifier: ETF_Cell_Id, for: indexPath) as! ETFCell
        let etf = etfArr[indexPath.row]
        cell.tickerLbl.text = etf.ticker
        cell.nameLbl.text = etf.name
        cell.priceLbl.text = "\(etf.percentChange ?? 0.00)"
        percentageChange(cell: cell, etfData: etf)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedETF = etfArr[indexPath.row]
        let etfDetailRef = storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTStatisticsVc) as! ETFStatisticsVC
        etfDetailRef.tickerName = selectedETF.ticker
        navigateTo(viewController: etfDetailRef)
        self.tabBarController?.tabBar.isHidden = true
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //  Based on TRENDS, Show variation between BULL and BEAR...
    func percentageChange(cell:ETFCell, etfData: ETFData){
        guard let trend = etfData.trend, let stars = etfData.stars, let starRate = Float(stars) else{return}
        switch trend {
        case "BULL":
            cell.prctgChangeLbl.textColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.prctgChangeLbl.text = "\(etfData.netChange!)%"
            cell.bullOrBearImg.image = #imageLiteral(resourceName: "bull")
            cell.isBullOrBrearLbl.textColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
        default:
            cell.prctgChangeLbl.textColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.prctgChangeLbl.text = "\(etfData.netChange!)%"
            cell.bullOrBearImg.image = #imageLiteral(resourceName: "bear")
            cell.isBullOrBrearLbl.textColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
        }
        cell.isBullOrBrearLbl.text = etfData.trend
        
        // Show star rating based on STAR Value ⭐️⭐️⭐️⭐️
        var starsRef: Float?
        let fullImg = #imageLiteral(resourceName: "green-full")
        let halfImg = #imageLiteral(resourceName: "green-half")
        switch starRate {
        case 0...24:
            starsRef = starRate/5
            cell.starImg[0].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[1].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[2].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[3].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[4].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
        default:
            let bearStarRate = "\(starRate/5)"
            let removeMinus = bearStarRate.dropFirst()
            starsRef = Float(String(removeMinus))!
            cell.starImg[0].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[1].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[2].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[3].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[4].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
        }
        
        func getStarImage(_ starNumber: Float, forRating rating: Float) -> UIImage {
            if rating >= starNumber {
                return fullImg
            } else if rating + 0.5 == starNumber {
                return halfImg
            } else {
                return #imageLiteral(resourceName: "star-outline")
            }
        }
        if let ourRating = starsRef {
            cell.starImg[0].image = getStarImage(1, forRating: ourRating)
            cell.starImg[1].image = getStarImage(2, forRating: ourRating)
            cell.starImg[2].image = getStarImage(3, forRating: ourRating)
            cell.starImg[3].image = getStarImage(4, forRating: ourRating)
            cell.starImg[4].image = getStarImage(5, forRating: ourRating)
        }
        
        for (index, imageView) in cell.starImg!.enumerated() {
            imageView.image = getStarImage(Float(index + 1), forRating: starsRef!)
        }
    }
    
    
    //MARK:- TableView Delegate methods...
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 16, y: 0, width: view.frame.size.width-16, height: 40))
        headerView.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.7546583851)
        headerView.layer.shadowOffset = CGSize(width: 0, height: 10)
        headerView.layer.shadowRadius = 5
        headerView.layer.shadowColor = UIColor.black.cgColor
        headerView.layer.shadowOpacity = 0.1
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Exchange Traded Funds"
        label.textColor  = .black
        label.font = UIFont(name: "Gilroy-Medium", size: 17)
        headerView.addSubview(label)
        label.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 20).isActive = true
        label.rightAnchor.constraint(equalTo: headerView.rightAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        return headerView
    }
    
    //MARK:- hide and Un-hide tabbar...
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.tabBarController?.tabBar.isHidden = true
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.tabBarController?.tabBar.isHidden = false
                print("Unhide")
            }, completion: nil)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.tabBarController?.tabBar.isHidden = false
    }
}

// MARK: MY PORTFOLIO DELEGATE AND DATASOURCE...

extension BTHomeVC: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myPortfolioArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: portfolio_cell_id, for: indexPath) as! BTMyPortfolioCell
        let portfolio = myPortfolioArr[indexPath.row]
        switch indexPath.row {
        case 0:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.4117647059, green: 0.4392156863, blue: 0.9607843137, alpha: 1)
        case 1:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.2156862745, green: 0.7882352941, blue: 0.5058823529, alpha: 1)
        case 2:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.2941176471, blue: 0.4549019608, alpha: 1)
        case 3:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.2274509804, blue: 0.9137254902, alpha: 1)
        case 4:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.8470588235, blue: 0.7568627451, alpha: 1)
        case 5:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        default:
            cell.portfolioBGV.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1882352941, blue: 0.2862745098, alpha: 1)
        }
        cell.portfolioName.text = portfolio.portfolioName//portfolio.portfolioName
        cell.gainLbl.text = "0000" //portfolio.gain
        
        return cell
    }
}

extension BTBaseViewController{
    func converMillsecToDate(_ sec: Int?) -> Int{
        let days = sec!/86400
        return days
    }
}
