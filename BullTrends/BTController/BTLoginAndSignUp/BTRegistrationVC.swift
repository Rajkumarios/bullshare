//
//  BTRegistrationVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 08/12/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import UIKit

class BTRegistrationVC: BTBaseViewController {
    
    var warningMessageStr: String = ""
    var isChecked: Bool?
    
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var nameAliasTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var RegistrationScroll: UIScrollView!
    @IBOutlet weak var registrationBtn: BTButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addObservers()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObservers()
    }
    
    func updateUI(){
        fullNameTF.delegate = self;  nameAliasTF.delegate = self
        emailTF.delegate = self;     mobileNumberTF.delegate = self;    passwordTF.delegate = self
        isChecked = false
    }
    
    @IBAction func checkBtnTapped(_ sender: UIButton) {
       sender.isSelected = !sender.isSelected
        if sender.isSelected == true{
        isChecked = true
        }else{
        isChecked = false
        }
    }
    
    @IBAction func termsNconditionsBtnTapped(_ sender: UIButton) {
        presentVCModally(storyboard: StoryboardId.BTTermsAndConditionsVc)
    }
    
    @IBAction func registrationBtnTapped(_ sender: BTButton) {
        warningMessageStr.removeAll()
          guard let fullName = fullNameTF.text, let nameAlias = nameAliasTF.text, let email = emailTF.text, var mobileNo = mobileNumberTF.text, let password = passwordTF.text else {return}
        guard let country = UserDefaults.standard.value(forKey: "country") as? String else{return}
        switch country {
        case "IN":
            mobileNo = "+91\(mobileNo)"
        default:
            mobileNo = "+1\(mobileNo)"
        }
        if isChecked == true{
            userRegistration(fullName: fullName, nameAlias: nameAlias, email: email, mobile: mobileNo, password: password)
        }else{
            showWarnig(title: "Terms & Conditions", message: "Please accept Terms&Conditions.")
        }
    }
    
    func userRegistration(fullName: String, nameAlias: String, email: String, mobile: String, password: String){
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let parameters = [
            "email": email,
            "fullName": fullName,
            "mbAlias": nameAlias,
            "password": password,
            "phoneNumber": mobile] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: parameters, url: API.kRegistration, type: BTRegistration.self) { (registration) in
            guard let statusCode = registration.statusCode else{return}
            
            if statusCode == 403{
                if let errorList = registration.errorList{
                    for i in 0 ..< errorList.count{
                        self.warningMessageStr.append("\(errorList[i])\n")
                    }
                    self.showWarnig(title: "Registration", message: self.warningMessageStr)
                }
            }else{
                let countryVcRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTCountrySelection)
                self.navigateTo(viewController:countryVcRef!)
            }
        }
    }
}

// Textfield Delegate menthods...
extension BTRegistrationVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fullNameTF{
            textField.resignFirstResponder()
            nameAliasTF.becomeFirstResponder()
        }else if textField == nameAliasTF{
            textField.resignFirstResponder()
            emailTF.becomeFirstResponder()
        }else if textField == emailTF{
            textField.resignFirstResponder()
            mobileNumberTF.becomeFirstResponder()
        }else if textField == mobileNumberTF{
            textField.resignFirstResponder()
            passwordTF.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}

// Keyboard Notifications...
extension BTRegistrationVC {
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide), name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    func removeObservers(){
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo, let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else{return}
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height+30, right: 0)
        RegistrationScroll.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        RegistrationScroll.contentInset = .zero
        RegistrationScroll.contentOffset = .zero
    }
}
