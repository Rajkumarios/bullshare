//
//  BTNewsDetailPage.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 13/05/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit
import WebKit

class BTNewsDetailPage: BTBaseViewController,WKNavigationDelegate {
    
    var newsDetailURL: String?
    @IBOutlet weak var newsDetailWV: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsDetailWV.navigationDelegate = self
        loadWebView()
    }
    func loadWebView(){
        let url = URL(string: newsDetailURL ?? "http://wwwqa.bull-trends.com/BullTrends/app/index.html#!/")
        let requestObj = URLRequest(url: url! as URL)
        newsDetailWV.load(requestObj)
        hideActivity()
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivty()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideActivity()
    }
}
