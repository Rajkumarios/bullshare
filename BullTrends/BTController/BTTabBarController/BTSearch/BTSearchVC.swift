//
//  BTSearchVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 04/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTSearchVC: BTBaseViewController,UITextFieldDelegate {
    
    private let SEARCH_CELL = "SearchCell"
    var searchArr = [ETFData]()

    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchTV: UITableView!
    @IBOutlet var bullBearEtf: [BTButtonCorners]!
    @IBOutlet weak var searchView: BTView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        searchView.alpha = 0
    }
    
    func updateUI(){
        searchTF.delegate = self
        getSearchData(searchTxt: "BULL")
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == searchTF && textField.text!.count > 0{
            getSearchData(searchTxt: searchTF.text!)
            textField.resignFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: .letters) != nil || string == ""{
            return true
        }else {
            return false
        }
    }
    @IBAction func onSearchbtnTapped(_ sender: UIButton) {
        searchView.alpha = 1
        searchTF.becomeFirstResponder()
    }
    @IBAction func onBullBearETFBtnTapped(_ sender: BTButtonCorners) {
        searchView.alpha = 0
        searchTF.resignFirstResponder()
        switch sender.tag {
        case 0:
            setBullBearETFUI(button: bullBearEtf[0], bgColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1), alpha: 1, titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            setBullBearETFUI(button: bullBearEtf[1], bgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), alpha: 0.5, titleColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1))
            setBullBearETFUI(button: bullBearEtf[2], bgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), alpha: 0.5, titleColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1))
            getSearchData(searchTxt: "BULL")

        case 1:
            setBullBearETFUI(button: bullBearEtf[0], bgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), alpha: 0.5, titleColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1))
            setBullBearETFUI(button: bullBearEtf[1], bgColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1), alpha: 1, titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            setBullBearETFUI(button: bullBearEtf[2], bgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), alpha: 0.5, titleColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1))
            getSearchData(searchTxt: "BEAR")

        default:
            setBullBearETFUI(button: bullBearEtf[0], bgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), alpha: 0.5, titleColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1))
            setBullBearETFUI(button: bullBearEtf[1], bgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), alpha: 0.5, titleColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1))
            setBullBearETFUI(button: bullBearEtf[2], bgColor: #colorLiteral(red: 0.1294117647, green: 0.1874444485, blue: 0.2855820656, alpha: 1), alpha: 1, titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            getSearchData(searchTxt: "ETF")
        }
    }
    func setBullBearETFUI(button: BTButtonCorners, bgColor: UIColor,alpha: CGFloat, titleColor: UIColor){
        button.backgroundColor = bgColor
        button.alpha = alpha
        button.setTitleColor(titleColor, for: .normal)
    }
    
    func getSearchData(searchTxt: String){
        guard let country = UserDefaults.standard.value(forKey: "country") as? String else{return}
        
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let postParams =
            ["country": "USA",
             "token": User.accessToken,
             "ticker": searchTxt,
             "maxRows": 50] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: postParams as [String : Any], url: API.KTrendSearch, type: Etf.self) { (searchData) in
            print(searchData.data as Any)
            
            if let searchedData = searchData.data{
                self.searchArr.removeAll()
                self.searchArr = searchedData
                
                print(self.searchArr.count)
                DispatchQueue.main.async {
                    self.searchTV.reloadData()
                    if self.searchArr.count == 0{
                        self.showWarnig(title: "No data found", message: " ")
                    }
                }
            }else{
                return
            }
        }
    }
    @IBAction func onAdvancedSearchBtnTapped(_ sender: UIButton) {
        let advancedSearchRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTAdvancedSearchVc) as! BTAdvancedSearchVc
        self.navigateTo(viewController: advancedSearchRef)
    }
}


extension BTSearchVC: UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- TableView Data source methods..
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchTV.dequeueReusableCell(withIdentifier: SEARCH_CELL, for: indexPath) as! SearchCell
        let searched = searchArr[indexPath.row]
        cell.tickerLbl.text = searched.ticker
        cell.nameLbl.text = searched.name
        cell.priceLbl.text = "\(searched.percentChange ?? 0.00)"
        percentageChange(cell: cell, searchedData: searched)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedETF = searchArr[indexPath.row]
        let etfDetailRef = storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTStatisticsVc) as! ETFStatisticsVC
        etfDetailRef.tickerName = selectedETF.ticker
        navigateTo(viewController: etfDetailRef)
        self.tabBarController?.tabBar.isHidden = true
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //  Based on TRENDS, Show variation between BULL and BEAR...
    func percentageChange(cell:SearchCell, searchedData: ETFData){
        guard let trend = searchedData.trend, let stars = searchedData.stars, let starRate = Float(stars) else{return}
        switch trend {
        case "BULL":
            cell.prctgChangeLbl.textColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.prctgChangeLbl.text = "\(searchedData.netChange ?? 0.00)%"
            cell.bullOrBearImg.image = #imageLiteral(resourceName: "bull")
            cell.isBullOrBrearLbl.textColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
        default:
            cell.prctgChangeLbl.textColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.prctgChangeLbl.text = "\(searchedData.netChange ?? 0.00)%"
            cell.bullOrBearImg.image = #imageLiteral(resourceName: "bear")
            cell.isBullOrBrearLbl.textColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
        }
        cell.isBullOrBrearLbl.text = searchedData.trend
        
        // Show star rating based on STAR Value ⭐️⭐️⭐️⭐️
        var starsRef: Float?
        let fullImg = #imageLiteral(resourceName: "green-full")
        let halfImg = #imageLiteral(resourceName: "green-half")
        switch starRate {
        case 0...24:
            starsRef = starRate/5
            cell.starImg[0].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[1].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[2].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[3].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
            cell.starImg[4].tintColor = #colorLiteral(red: 0.3438521028, green: 0.7418661714, blue: 0.2244175375, alpha: 1)
        default:
            let bearStarRate = "\(starRate/5)"
            let removeMinus = bearStarRate.dropFirst()
            starsRef = Float(String(removeMinus))!
            cell.starImg[0].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[1].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[2].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[3].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
            cell.starImg[4].tintColor = #colorLiteral(red: 0.8850280643, green: 0.2072871029, blue: 0.1874347031, alpha: 1)
        }
        
        func getStarImage(_ starNumber: Float, forRating rating: Float) -> UIImage {
            if rating >= starNumber {
                return fullImg
            } else if rating + 0.5 == starNumber {
                return halfImg
            } else {
                return #imageLiteral(resourceName: "star-outline")
            }
        }
        if let ourRating = starsRef {
            cell.starImg[0].image = getStarImage(1, forRating: ourRating)
            cell.starImg[1].image = getStarImage(2, forRating: ourRating)
            cell.starImg[2].image = getStarImage(3, forRating: ourRating)
            cell.starImg[3].image = getStarImage(4, forRating: ourRating)
            cell.starImg[4].image = getStarImage(5, forRating: ourRating)
        }
        
        for (index, imageView) in cell.starImg!.enumerated() {
            imageView.image = getStarImage(Float(index + 1), forRating: starsRef!)
        }
    }
    
    //MARK:- hide and Un-hide tabbar...
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            self.searchTF.resignFirstResponder()
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.tabBarController?.tabBar.isHidden = true
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.tabBarController?.tabBar.isHidden = false
                print("Unhide")
            }, completion: nil)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.tabBarController?.tabBar.isHidden = false
    }
}
