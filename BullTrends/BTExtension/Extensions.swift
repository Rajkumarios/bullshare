//
//  Extensions.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 28/11/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import Foundation
import  UIKit

// MARK:- View Gradient...
class BTView: UIView{
    override func didMoveToWindow() {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = 0.06
        self.layer.shadowColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        self.layer.shadowOffset = CGSize(width: 0, height: 6)
        self.layer.shadowRadius = 5
    }
}
// MARK:- View Corners and minimum radius...
class BTViewDefault: UIView{
    override func didMoveToWindow() {
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = 0.2
        self.layer.shadowColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 8
    }
}

// MARK:-  View Corner-Radius...
class BTViewCorners: UIView{
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        
    }
}
class BTViewTopCorners: UIView {
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 16
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.clipsToBounds = true
    }
}

// MARK:- UIButton Corner-Radius...
class BTButton: BTButtonCorners{
    
    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
        self.setGradientBackgroundColor(colorOne: #colorLiteral(red: 1, green: 0.5098039216, blue: 0.1803921569, alpha: 1), colorTwo: #colorLiteral(red: 0.8925027251, green: 0.2142743766, blue: 0.1896360219, alpha: 1), button: self)
    }
    
    func setGradientBackgroundColor(colorOne: UIColor,colorTwo: UIColor,button: UIButton){
        let gradientLayer = CAGradientLayer.init()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 0.6]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.3)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        layer.insertSublayer(gradientLayer, at: 0)
        
        if let imageView = button.imageView {
            button.bringSubview(toFront: imageView)
        }
    }
}

// MARK:- UIButton Corner-Radius...
class BTButtonCorners: UIButton {
    
    override func didMoveToWindow() {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowColor = #colorLiteral(red: 0.2666666667, green: 0.3764705882, blue: 0.6274509804, alpha: 1)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 5
    }
}

// MARK:- UIButton Shadow...
class BTButtonShadow: UIButton {
    
    override func didMoveToWindow() {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderColor = #colorLiteral(red: 0.1294117647, green: 0.1882352941, blue: 0.2862745098, alpha: 0.0983113354)
        self.layer.borderWidth = 1
    }
}


extension UIViewController{
    // Show Warning messages.. make toast...
    func showWarnig(title: String, message: String){
        var style = ToastStyle()
        style.cornerRadius = 0
        style.displayShadow = true
        style.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        style.shadowOffset = CGSize(width: 0, height: 4)
        style.maxWidthPercentage = 1.0
        style.messageFont = UIFont(name: "Gilroy-Regular", size: 13)!
        style.messageAlignment = .left
        style.titleFont = UIFont(name: "Gilroy-SemiBold", size: 15)!
        style.titleAlignment = .left
        DispatchQueue.main.async {
             self.view.makeToast(message, duration: 2.5, position: .top, title: title, image: #imageLiteral(resourceName: "icon"), style: style, completion: nil)
        }
    }
    // When touches began... resign first responder/keyboard hide...
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        self.view.hideAllToasts()
    }
    //Activity indicator will call... When service called...
    func showActivty(){
        DispatchQueue.main.async {
            self.view.makeToastActivity(.center)
        }
    }
    func hideActivity(){
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        }
    }
}
