//
//  SearchCell.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 11/05/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    
    @IBOutlet weak var tickerLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var prctgChangeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var bullOrBearImg: UIImageView!
    @IBOutlet weak var isBullOrBrearLbl: UILabel!
    @IBOutlet var starImg: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
