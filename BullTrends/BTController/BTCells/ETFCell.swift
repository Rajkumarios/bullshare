//
//  ETFCell.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 14/02/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class ETFCell: UITableViewCell {

    @IBOutlet weak var tickerLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var prctgChangeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var bullOrBearImg: UIImageView!
    @IBOutlet weak var isBullOrBrearLbl: UILabel!
    @IBOutlet var starImg: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
