//
//  NewsCell.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 10/03/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var newTitleLbl: UILabel!
    @IBOutlet weak var newsDetailLbl: UILabel!
    @IBOutlet weak var newsDateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
