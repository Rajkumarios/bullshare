//
//  BTOTPVC.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 07/03/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTOTPVC: BTBaseViewController {
    private var warningMessageStr = String()
    var countdownTimer: Timer!
    var totalTime = 29
    let user_id = UserDefaults.standard.value(forKey: "user_forgot") as! String

    @IBOutlet var OTPTF: UITextField!
    @IBOutlet weak var resendOTPBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    func updateUI(){
        OTPTF.becomeFirstResponder()
        OTPTF.delegate = self
        startTimer()
    }
    
    @IBAction func resendOTP(_ sender: UIButton) {
        passwordChangeRequest()
        totalTime = 29
        startTimer()
    }
    // start timer...
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        resendOTPBtn.setTitle("Resend in\(timeFormatted(totalTime))", for: .normal)
        resendOTPBtn.isEnabled = false
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    // end timer...
    func endTimer() {
        countdownTimer.invalidate()
        resendOTPBtn.isEnabled = true
        resendOTPBtn.setTitle("Resend OTP?", for: .normal)
    }
    // time formatting...
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d : %02d", minutes, seconds)
    }
    
    @IBAction func VerifyOTP(_ sender: UIButton) {
        warningMessageStr = ""
        checkOTP()
    }
    
    func checkOTP(){
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let parameters = [
            "password": "Bull@123",
            "confirmCode": OTPTF.text ?? "000000",
            "userid": user_id ] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: parameters, url: API.kPassword, type: ChangePassword.self) { (pswdChangeRequest) in
            guard let statusCode = pswdChangeRequest.statusCode else{return}
            
            if statusCode == 200{
                DispatchQueue.main.async {
                    let createNewPswdRef = self.storyboard?.instantiateViewController(withIdentifier: StoryboardId.BTCreateNewPasswordVC) as! BTCreateNewPasswordVC
                    createNewPswdRef.getOTPFromUser = self.OTPTF.text
                    self.navigateTo(viewController:createNewPswdRef)
                }
            }else{
                if let errorList = pswdChangeRequest.errorList{
                    for i in 0 ..< errorList.count{
                        self.warningMessageStr.append("\(errorList[i])\n")
                    }
                    DispatchQueue.main.async {
                        self.showWarnig(title: "OTP Verification", message: self.warningMessageStr)
                    }
                }
            }
        }
    }
}

extension BTOTPVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    // Password Change Request...
    func passwordChangeRequest(){
        let headers: [String: String] = [Headers.authorization : Authorization.Registration,
                                         Headers.contentType: ContentType.Json]
        let parameters = [
            "sendSms": true,
            "sendEmail": true,
            "confirmSMS": true,
            "userid": user_id] as [String : Any]
        
        serviceWithToken(headers: headers, parmas: parameters, url: API.kPasswordChange, type: ChangePassword.self) { (pswdChangeRequest) in
            guard let statusCode = pswdChangeRequest.statusCode else{return}
            
            if statusCode == 403{
                if let errorList = pswdChangeRequest.errorList{
                    for i in 0 ..< errorList.count{
                        self.warningMessageStr.append("\(errorList[i])\n")
                    }
                    DispatchQueue.main.async {
                        self.showWarnig(title: "Password Change", message: self.warningMessageStr)
                    }
                }
            }else{
                // OTP has been sent successfully..
            }
            
        }
    }
}
