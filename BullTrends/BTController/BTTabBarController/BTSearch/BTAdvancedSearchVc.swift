//
//  BTAdvancedSearchVc.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 21/05/19.
//  Copyright © 2019 BullTrends. All rights reserved.
//

import UIKit

class BTAdvancedSearchVc: BTBaseViewController {

    @IBOutlet weak var priceRangeSlider: UISlider!
    @IBOutlet weak var priceMaxValueLbl: UILabel!
    @IBOutlet weak var priceSliderValueLbl: UILabel!
    
    @IBOutlet weak var trendSlider: UISlider!
    @IBOutlet weak var trendSliderValueLbl: UILabel!
    
    @IBOutlet weak var gainSlider: UISlider!
    @IBOutlet weak var gainSliderValueLbl: UILabel!
    
    @IBOutlet weak var dividendSlider: UISlider!
    @IBOutlet weak var dividendSliderValue: UILabel!
    @IBOutlet weak var dividendMaxValueLbl: UILabel!
    
    @IBOutlet weak var fScoreSlider: UISlider!
    @IBOutlet weak var filterSliderValueLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func dragSlider(_ sender: UISlider) {
        
    }
    @IBAction func onResetBtnTapped(_ sender: UIButton) {
    }
    
    @IBAction func applyBtnTapped(_ sender: BTButton) {
    }
}
