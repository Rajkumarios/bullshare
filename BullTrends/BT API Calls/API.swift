//
//  API.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 08/12/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    
    // MARK:- GET METHOD
    func getServiceData<T: Codable>(serviceName: String, type: T.Type, completionHandler: @escaping(_ detaills: T) -> Void ){
        
        guard let url = URL(string:serviceName) else{return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let serviceData = data else{
                guard let serviceError = error else{return}
                self.showWarnig(title: "", message: (serviceError.localizedDescription))
                return
            }
            do{
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let dataModel = try decoder.decode(type, from: serviceData)
                completionHandler(dataModel)
                print(dataModel)

            }catch let parsingError{
                self.showWarnig(title: "", message: (parsingError.localizedDescription))
            }
            }.resume()
    }
    
     func getServiceCallWith<T: Codable>(header:[String: Any], serviceURL: String, type: T.Type, completionHandler: @escaping(_ detaills: T) -> Void ){
            showActivty()
        guard let url = URL(string: serviceURL) else{return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let serviceData = data,error == nil  else{
                self.showWarnig(title: "", message: (error?.localizedDescription)!)
                return
            }
            do{
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let decodedData = try decoder.decode(type, from: serviceData)
                completionHandler(decodedData)
                print(decodedData)

                self.hideActivity()
            }catch let parsingError{
                self.showWarnig(title: "", message: (parsingError.localizedDescription))
            }
            }.resume()
    }
    
    // MARK:- POST AND GET DATA FROM SERVER
    
        func postServiceCallWith<T: Codable>(header: [String: Any]?, serviceURL: String, params: String, type: T.Type, completionHandler: @escaping (_ details: T) -> Void){
            showActivty()
            guard  let url = URL(string: serviceURL) else {return}
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"
            urlRequest.allHTTPHeaderFields = header as? [String : String]
            urlRequest.httpBody = params.data(using: .utf8)
            
            URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                
                guard let dataResponse = data,error == nil else{
                    guard let serviceError = error else{return}
                    self.showWarnig(title: "", message: (serviceError.localizedDescription))
                    return
                }
                do{
                    let  decoder = JSONDecoder()
                   decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let decodedData = try decoder.decode(type, from: dataResponse)
                    print(decodedData)
                    
                    completionHandler(decodedData)
                    self.hideActivity()
                }catch let parsingError{
                    self.showWarnig(title: "", message: (parsingError.localizedDescription))
                }
                }.resume()
        }
    
    func serviceWithToken<T: Codable>(headers: [String: Any], parmas: [String: Any], url:String,type: T.Type, completionHandler: @escaping (_ details: T) -> Void){
        showActivty()
        let postData = try? JSONSerialization.data(withJSONObject: parmas, options: [])
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers as? [String : String]
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            guard let dataResponse = data,error == nil else{
                guard let serviceError = error else{return}
//                self.showWarnig(title: "", message: (serviceError.localizedDescription))
                self.presentVCModally(storyboard:  StoryboardId.BTInternetVc)

                return
            }
            do{
                let  decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let decodedData = try decoder.decode(type, from: dataResponse)
                print(decodedData)
                completionHandler(decodedData)
                self.hideActivity()
            }catch let parsingError{
                self.showWarnig(title: "", message: (parsingError.localizedDescription))
            }
        }).resume()
    }
    
    
    // MARK:- GET IMAGE DATA FROM SERVER
    
    func getImagesFromServer(imageURL: String?,completionHandler: @escaping(_ details: UIImage) -> Void){
        
        guard let img = imageURL, let url = URL(string: img)  else{return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                self.showWarnig(title: "", message: (err.localizedDescription))
            } else {
                if (response as? HTTPURLResponse) != nil {
                    if let imageData = data {
                        guard let image = UIImage(data: imageData)else {return}
                        DispatchQueue.main.async {
                            completionHandler(image)
                        }
                    } else {
                        self.showWarnig(title: "", message: "Image file is corrupted")
                    }
                } else {
                    self.showWarnig(title: "", message: "No response from server")
                }
            }
            }.resume()
    }
}
