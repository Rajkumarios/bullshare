//
//  BTLogin.swift
//  BullTrends
//
//  Created by Vasu Yarasu on 08/12/18.
//  Copyright © 2018 BullTrends. All rights reserved.
//

import Foundation

//For User Login
struct BTLogin: Codable {
    let accessToken, tokenType, refreshToken: String?
    let expiresIn: Int?
    let scope, emailAddress, phoneNumber, userIdentifier: String?
    let fullName: String?
    let errorInfo : ErrorInfo?
}

//Error response...
struct ErrorInfo: Codable {
    let error, error_description: String?
}

//For User Registration
struct BTRegistration: Codable {
    let statusCode: Int?
    let errorList: [String]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorList = "errorList"
    }
}

// For Change password request..
struct ChangePassword: Codable {
    let statusCode: Int?
    let errorList: [String]?
}
